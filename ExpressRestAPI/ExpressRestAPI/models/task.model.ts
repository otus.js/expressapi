export interface Task {
    id: number;
    title: string;
    code: string;
    correct_code: string;
}