export interface Run {
    id: number;
    user_id: number;
    task_id: number;
    user_code: string;
    language: string;
    dt: string;
    hashcode: string;
}