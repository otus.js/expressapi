export interface Comment {
    id: number;
    text: string;
    user_id: number;
    task_id: number;
    dt: string;
}