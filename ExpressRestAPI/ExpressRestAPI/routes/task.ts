/*
 * GET users listing.
 */
import { tasks } from "../repositorys/task.repository"

import express = require('express');

const router = express.Router();
/**
 * @swagger
 * tags:
 *   name: Tasks
 *   description: API for managing users
 */

/**
 * @swagger
 * /tasks:
 *   get:
 *     summary: Get a list of task.
 *     tags: [Tasks]
 *     responses:
 *       '200':
 *         description: A list of users.
 */
router.get("/", (req, res) => {
    res.json(tasks);
});

/**
 * @swagger
 * /tasks/{id}:
 *   get:
 *     summary: Get task by ID.
 *     tags: [Tasks]
 *     parameters:
 *     - in: path
 *       name: id
 *       required: true
 *       description: ID of the user.
 *       schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single user.
 */
router.get("/:id", (req, res) => {
    const taskId = Number(req.params.id);
    const foundTask = tasks.find((c) => c.id === taskId);
    if (foundTask == null) res.send(`Task id = ${taskId} not found`);
    else res.json(foundTask);
});

/**
 * @swagger
 * /tasks:
 *   post:
 *     summary: Create a JSONPlaceholder task.
 *     tags: [Tasks]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               title:
 *                 type: string
 *                 description: The task's title.
 *                 example:"Array task"
 *               code:
 *                 type: string
 *                 description: The task's code.
 *                 example: consol.log('Hello world');
 *               correct_code:
 *                 type: string
 *                 description: The task's correct_code.
 *     responses:
 *       201:
 *         description: A single user.
*/
router.post('/', function (req, res) {
    let items = tasks.map(item => item.id);

    let newId = items.length > 0 ? Math.max.apply(Math, items) + 1 : 1;

    let newItem = {
        id: newId,
        title: req.body.title,
        code: req.body.code,
        correct_code: req.body.correct_code
    }

    tasks.push(newItem);

    res.status(201).json({
        'message': "successfully created"
    });
});
/**
 * @swagger
 * /tasks:
 *   put:
 *     summary: Edit a JSONPlaceholder task.
 *     tags: [Tasks]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               id:
 *                 type: string
 *                 description: The user's id.
 *                 example: 
 *               title:
 *                 type: string
 *                 description: The user's name.
 *                 example: Leanne Graham
 *               code:
 *                 type: string
 *                 description: The task's code.
 *               correct_code:
 *                 type: string
 *                 description: The task's correct_code.
 *     responses:
 *       201:
 *         description: A single user.
*/
router.put('/', function (req, res) {
    let found = tasks.find(function (item) {
        return item.id === parseInt(req.body.id);
    });
    if (found) {
        let updateData = {
            id: found.id,
            title: req.body.title,
            code: req.body.code,
            correct_code: req.body.correct_code
        };

        let targetIndex = tasks.indexOf(found);

        tasks.splice(targetIndex, 1, updateData);

        res.status(201).json({ 'message': "data updated" });
    } else {
        res.status(404).json({
            'message': 'unable to insert data because data inserted not matched'
        });
    }
});

/**
 * @swagger
 * /tasks/{id}/delete:
 *   delete:
 *     summary: Delete task by ID.
 *     tags: [Tasks]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the task.
 *         schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single user.
 */
router.delete("/:id/delete", (req, res) => {
    const Index = tasks.findIndex((c) => c.id === Number(req.params.id));
    if (Index >= 0) tasks.splice(Index, 1);
    res.json(tasks);
});


export default router;