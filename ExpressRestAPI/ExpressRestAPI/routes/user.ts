/*
 * GET users listing.
 */
import { users } from "../repositorys/user.repository"
import { UserDTO } from "./userDto";

import express = require('express');

const router = express.Router();
/**
 * @swagger
 * tags:
 *   name: Users
 *   description: API for managing users
 */

/**
 * @swagger
 * /users:
 *   get:
 *     summary: Get a list of users.
 *     tags: [Users]
 *     responses:
 *       '200':
 *         description: A list of users.
 */
router.get("/", (req, res) => {
    res.json(users);
});

/**
 * @swagger
 * /users/{id}:
 *   get:
 *     summary: Get user by ID.
 *     tags: [Users]
 *     parameters:
 *     - in: path
 *       name: id
 *       required: true
 *       description: ID of the user.
 *       schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single user.
 */
router.get("/:id", (req, res) => {
    const userId = Number(req.params.id);
    const foundUser = users.find((c) => c.id === userId);
    if (foundUser == null) res.send(`User id = ${userId} not found` );
    else res.json(foundUser);
});

/**
 * @swagger
 * /users:
 *   post:
 *     summary: Create a JSONPlaceholder user.
 *     tags: [Users]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: The user's name.
 *                 example: Leanne Graham
 *               email:
 *                 type: string
 *                 description: The user's email.
 *                 example: L_Graham@ya.ru
 *               password:
 *                 type: string
 *                 description: The user's password.
 *     responses:
 *       201:
 *         description: A single user.
*/
router.post('/', function (req, res) {
    let items = users.map(item => item.id);

    let newId = items.length > 0 ? Math.max.apply(Math, items) + 1 : 1;

    let newItem = {
        id: newId,
        name: req.body.name,
        email: req.body.email,
        password: req.body.password
    }

    users.push(newItem);

    res.status(201).json({
        'message': "successfully created"
    });
});
/**
 * @swagger
 * /users:
 *   put:
 *     summary: Edit a JSONPlaceholder user.
 *     tags: [Users]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               id:
 *                 type: string
 *                 description: The user's id.
 *                 example: 
 *               name:
 *                 type: string
 *                 description: The user's name.
 *                 example: Leanne Graham
 *               email:
 *                 type: string
 *                 description: The user's email.
 *                 example: L_Graham@ya.ru
 *               password:
 *                 type: string
 *                 description: The user's password.
 *     responses:
 *       201:
 *         description: A single user.
*/
router.put('/', function (req, res) {
   let found = users.find(function (item) {
        return item.id === parseInt(req.body.id);
    });
    if (found) {
        let updateData = {
            id: found.id,
            name: req.body.name,
            email: req.body.email,
            password: req.body.password
        };

        let targetIndex = users.indexOf(found);

        users.splice(targetIndex, 1, updateData);

        res.status(201).json({ 'message': "data updated" });
    } else {
        res.status(404).json({
            'message': 'unable to insert data because data inserted not matched'
        });
    }
});

/**
 * @swagger
 * /users/{id}/delete:
 *   delete:
 *     summary: Delete user by ID.
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the user.
 *         schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single user.
 */
router.delete("/:id/delete", (req, res) => {
    const usrIndex = users.findIndex((c) => c.id === Number(req.params.id));
    if (usrIndex >=0) users.splice(usrIndex, 1);
    res.json(users);
});


export default router;