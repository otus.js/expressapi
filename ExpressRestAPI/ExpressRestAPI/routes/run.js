"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * GET users listing.
 */
const run_repository_1 = require("../repositorys/run.repository");
const express = require("express");
const router = express.Router();
/**
 * @swagger
 * tags:
 *   name: Runs
 *   description: API for managing runs
 */
/**
 * @swagger
 * /runs:
 *   get:
 *     summary: Get a list of runs.
 *     tags: [Runs]
 *     responses:
 *       '200':
 *         description: A list of users.
 */
router.get("/", (req, res) => {
    res.json(run_repository_1.runs);
});
/**
 * @swagger
 * /runs/{id}:
 *   get:
 *     summary: Get run by ID.
 *     tags: [Runs]
 *     parameters:
 *     - in: path
 *       name: id
 *       required: true
 *       description: ID of the run.
 *       schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single user.
 */
router.get("/:id", (req, res) => {
    const runId = Number(req.params.id);
    const foundRun = run_repository_1.runs.find((c) => c.id === runId);
    if (foundRun == null)
        res.send(`Run id = ${runId} not found`);
    else
        res.json(foundRun);
});
/**
 * @swagger
 * /runs:
 *   post:
 *     summary: Create a JSONPlaceholder run.
 *     tags: [Runs]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               user_id:
 *                 type: string
 *                 description: The user's id.
 *               task_id:
 *                 type: string
 *                 description: The task's id.
 *               user_code:
 *                 type: string
 *                 description: The user's code.
 *               language:
 *                 type: string
 *                 description: The user's code language.
 *               hashcode:
 *                 type: string
 *                 description: The task hashcode.

 *     responses:
 *       201:
 *         description: A single user.
*/
router.post('/', function (req, res) {
    let items = run_repository_1.runs.map(item => item.id);
    let newId = items.length > 0 ? Math.max.apply(Math, items) + 1 : 1;
    const currdate = new Date();
    let newItem = {
        id: newId,
        user_id: req.body.user_id,
        task_id: req.body.task_id,
        user_code: req.body.user_code,
        language: req.body.language,
        dt: currdate.toString(),
        hashcode: req.body.hashcode
    };
    run_repository_1.runs.push(newItem);
    res.status(201).json({
        'message': "successfully created"
    });
});
/**
 * @swagger
 * /runs:
 *   put:
 *     summary: Edit a JSONPlaceholder runs.
 *     tags: [Runs]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               id:
 *                 type: string
 *                 description: The user's id.
 *                 example:
 *               user_id:
 *                 type: string
 *                 description: The user's id.
 *               task_id:
 *                 type: string
 *                 description: The task's id.
 *               user_code:
 *                 type: string
 *                 description: The user's code.
 *               language:
 *                 type: string
 *                 description: The user's code language.
 *               hashcode:
 *                 type: string
 *                 description: The task hashcode.
 *     responses:
 *       201:
 *         description: A single user.
*/
router.put('/', function (req, res) {
    let found = run_repository_1.runs.find(function (item) {
        return item.id === parseInt(req.body.id);
    });
    if (found) {
        const currdate = new Date();
        let updateData = {
            id: found.id,
            user_id: req.body.user_id,
            task_id: req.body.task_id,
            user_code: req.body.user_code,
            language: req.body.language,
            dt: currdate.toString(),
            hashcode: req.body.hashcode
        };
        let targetIndex = run_repository_1.runs.indexOf(found);
        run_repository_1.runs.splice(targetIndex, 1, updateData);
        res.status(201).json({ 'message': "data updated" });
    }
    else {
        res.status(404).json({
            'message': 'unable to insert data because data inserted not matched'
        });
    }
});
/**
 * @swagger
 * /runs/{id}/delete:
 *   delete:
 *     summary: Delete run by ID.
 *     tags: [Runs]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the user.
 *         schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single user.
 */
router.delete("/:id/delete", (req, res) => {
    const usrIndex = run_repository_1.runs.findIndex((c) => c.id === Number(req.params.id));
    if (usrIndex >= 0)
        run_repository_1.runs.splice(usrIndex, 1);
    res.json(run_repository_1.runs);
});
exports.default = router;
//# sourceMappingURL=run.js.map