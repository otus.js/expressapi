"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * GET comments listing.
 */
const comment_repository_1 = require("../repositorys/comment.repository");
const express = require("express");
const router = express.Router();
/**
 * @swagger
 * tags:
 *   name: Comments
 *   description: API for managing users
 */
/**
 * @swagger
 * /comments:
 *   get:
 *     summary: Get a list of comments.
 *     tags: [Comments]
 *     responses:
 *       '200':
 *         description: A list of comments.
 */
router.get("/", (req, res) => {
    res.json(comment_repository_1.comments);
});
/**
 * @swagger
 * /comments/{id}:
 *   get:
 *     summary: Get comment by ID.
 *     tags: [Comments]
 *     parameters:
 *     - in: path
 *       name: id
 *       required: true
 *       description: ID of the comment.
 *       schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single user.
 */
router.get("/:id", (req, res) => {
    const Id = Number(req.params.id);
    const foundObj = comment_repository_1.comments.find((c) => c.id === Id);
    if (foundObj == null)
        res.send(`User id = ${Id} not found`);
    else
        res.json(foundObj);
});
/**
 * @swagger
 * /comments:
 *   post:
 *     summary: Create a JSONPlaceholder comment.
 *     tags: [Comments]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               text:
 *                 type: string
 *                 description: The content's description.
 *                 example: description task
 *               user_id:
 *                 type: string
 *                 description: The user's id.
 *               task_id:
 *                 type: string
 *                 description: The task's id.
 *     responses:
 *       201:
 *         description: A single content.
*/
router.post('/', function (req, res) {
    let items = comment_repository_1.comments.map(item => item.id);
    let newId = items.length > 0 ? Math.max.apply(Math, items) + 1 : 1;
    const dat = new Date();
    let newItem = {
        id: newId,
        text: req.body.text,
        user_id: req.body.user_id,
        task_id: req.body.task_id,
        dt: dat.toDateString()
    };
    comment_repository_1.comments.push(newItem);
    res.status(201).json({
        'message': "successfully created"
    });
});
/**
 * @swagger
 * /comments:
 *   put:
 *     summary: Edit a JSONPlaceholder comment.
 *     tags: [Comments]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               id:
 *                 type: string
 *                 description: The comment's id.
 *                 example:
 *               text:
 *                 type: string
 *                 description: The content's description.
 *                 example: description task
 *               user_id:
 *                 type: string
 *                 description: The user's id.
 *               task_id:
 *                 type: string
 *                 description: The task's id.
 *     responses:
 *       201:
 *         description: A single user.
*/
router.put('/', function (req, res) {
    let found = comment_repository_1.comments.find(function (item) {
        return item.id === parseInt(req.body.id);
    });
    if (found) {
        const dat = new Date();
        let updateData = {
            id: found.id,
            text: req.body.text,
            user_id: req.body.user_id,
            task_id: req.body.task_id,
            dt: dat.toDateString()
        };
        let targetIndex = comment_repository_1.comments.indexOf(found);
        comment_repository_1.comments.splice(targetIndex, 1, updateData);
        res.status(201).json({ 'message': "data updated" });
    }
    else {
        res.status(404).json({
            'message': 'unable to insert data because data inserted not matched'
        });
    }
});
/**
 * @swagger
 * /comments/{id}/delete:
 *   delete:
 *     summary: Delete comment by ID.
 *     tags: [Comments]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the user.
 *         schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single user.
 */
router.delete("/:id/delete", (req, res) => {
    const usrIndex = comment_repository_1.comments.findIndex((c) => c.id === Number(req.params.id));
    if (usrIndex >= 0)
        comment_repository_1.comments.splice(usrIndex, 1);
    res.json(comment_repository_1.comments);
});
exports.default = router;
//# sourceMappingURL=comment.js.map